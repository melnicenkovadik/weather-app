window.addEventListener('load', () => {
  let long;
  let lat;
  let temperatureDescription = document.querySelector('.temperature-description')
  let temperatureDegree = document.querySelector('.temperature-degree')
  let temperatureFeelsLikeIs = document.querySelector('.temperature-feels_like')
  let temperatureTempMaxIs = document.querySelector('.temperature-temp-max')
  let temperatureTempMin = document.querySelector('.temperature-temp-min')
  let locationTimezone = document.querySelector('.location-timezone')
  let locationCountry = document.querySelector('.location-country')
  let so = document.querySelector('.temperature-description')
  let temperature = document.querySelector('.temperature')
  let span = document.querySelector('.temperature span');
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(position => {
      long = position.coords.longitude;
      lat = position.coords.latitude;

      const proxy = `https://cors-anywhere.herokuapp.com`
      const api = `${proxy}/api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=0c8bae2cb5e1ca6e194d832b40336390`
      console.log(api);
      fetch(api)
        .then(response => {
          return response.json()
        })
        .then(data => {
          // set Dom elements from api
          const icon = data.weather[0].main
          console.log(icon);
          temperatureDegree.insertAdjacentHTML("beforeend",toCelsium(data.main.temp) )
          temperatureDescription.textContent = data.weather[0].description
          temperatureTempMaxIs.textContent = toCelsium(data.main.temp_max)
          temperatureFeelsLikeIs.textContent = toCelsium(data.main.feels_like)
          temperatureTempMin.textContent = toCelsium(data.main.temp_min)
          locationTimezone.textContent = data.name
          locationCountry.textContent = data.sys.country
          const desc = data.weather[0].description
          so.textContent = desc.charAt(0).toUpperCase() + desc.substr(1);
          setIcons(icon, document.querySelector(".icon"))


        })

    });


  }

  function toCelsium(kelvin) {
    const str = Math.floor(kelvin - 273.15)
    // const first = str.charAt(0)+ str.charAt(1)
    // const two =  first + "," + str.substr(1)
    const s = `${str} °`.toString()
    return s
  }

  function setIcons(icon, iconId) {
    const skycons = new Skycons({color: "white"})
    const currentIcon = icon.replace(/-/g, "_").toUpperCase();
    skycons.play();
    console.log(currentIcon);

    return skycons.set(iconId, Skycons[currentIcon])
  }
});
